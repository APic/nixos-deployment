{ config, pkgs, lib, ... }:

{
  imports = [
    "${fetchTarball "https://github.com/NixOS/nixos-hardware/archive/9886a06e4745edb31587d0e9481ad82d35f0d593.tar.gz"}/raspberry-pi/4"
  ];

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/LOUNGEPI_NIXOS";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };

  networking = {
    hostName = "loungepi";
    useNetworkd = true;
    useDHCP = false;
    interfaces.eth0.useDHCP = true;
    interfaces.wlan0.useDHCP = true;
    wireless = {
      enable = true;
      networks."muccc.legacy-5GHz".psk = "haileris";
      interfaces = [ "wlan0" ];
    };
    firewall = {
      trustedInterfaces = [ "wlan0" ]; # FIXME
    };
  };

  systemd.network.networks."40-wlan0" = {
    networkConfig.LLMNR = false;
  };
  systemd.network.networks."40-eth0" = {
    linkConfig.RequiredForOnline = false;
    networkConfig.LLMNR = false;
    dhcpV4Config.RouteMetric = 23;
    dhcpV6Config.RouteMetric = 23;
    extraConfig = ''
      [IPv6AcceptRA]
      RouteMetric=23
    '';
  };

  boot.extraModprobeConfig = ''
    options snd_bcm2835 enable_headphones=1
  '';

  zramSwap.memoryPercent = 100;

  environment.systemPackages = with pkgs; [ colmena lm_sensors ];
  programs.vim.defaultEditor = true;
  environment.sessionVariables.PAN_MESA_DEBUG = "gl3";

  # FIXME
  services.openssh.passwordAuthentication = true;

  users = {
    mutableUsers = false;
    users.lounge = {
      isNormalUser = true;
      password = "alarm";
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFJY+/tAXZFm9U+nJt0kKo6e/TrYiH7E49n0ktbuF5I6 fpletz@fpine"
      ];
    };
    users.root.openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFJY+/tAXZFm9U+nJt0kKo6e/TrYiH7E49n0ktbuF5I6 fpletz@fpine"
    ];
  };

  home-manager.users.lounge = { pkgs, ... }: {
    home.stateVersion = "22.05";
    #programs.bash.enable = true;
    programs.tmux.enable = true;
    programs.mosh.enable = true;
    programs.ncmpcpp.enable = true;
    programs.foot = {
      enable = true;
      settings = {
        main = {
          term = "xterm-256color";
          font = "Fira Code:size=18";
        };
      };
    };
    services.mpd.enable = true;
    wayland.windowManager.sway = {
      enable = true;
      wrapperFeatures.gtk = true;
      config = {
        modifier = "Mod4";
        bars = [];
        startup = [
	        { command = "${pkgs.foot}/bin/foot tmuxp load -y start"; }
        ];
      };
    };
    programs.waybar = {
      enable = true;
      systemd.enable = true;
      #style = ''
      #  * {
      #    font-family: Fira Code;
      #    font-size: 20px;
      #  }
      #'';
    };
    programs.mpv = {
      enable = true;
      config = {
        ytdl-format = "bestvideo[ext=webm][height<=?1080]+bestaudio[acodec=opus]/bestvideo[height<=?1080]+bestaudio/best";
      };
    };
    home.packages = with pkgs; [ youtube-dl pulsemixer cmatrix tmuxp ];
  };

  # Enable GPU acceleration
  hardware.raspberry-pi."4".fkms-3d.enable = true;
  hardware.raspberry-pi."4".audio.enable = true;
  hardware.raspberry-pi."4".dwc2.enable = true;

  sound.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };
  security.rtkit.enable = true;

  programs.sway.enable = true;
  fonts.fonts = with pkgs; [ fira fira-code fira-code-symbols fira-mono powerline-fonts font-awesome_4 font-awesome_5 ];

  services.greetd = {
    enable = true;
    settings = {
      initial_session = {
        command = "sway";
        user = "lounge";
      };
      default_session = {
        command = "${lib.makeBinPath [pkgs.greetd.tuigreet]}/tuigreet --time --cmd sway";
        user = "greeter";
      };
    };
  };

  systemd.services.gitlab-runner.serviceConfig.DynamicUser = lib.mkForce false;
  services.gitlab-runner = {
    enable = true;
    concurrent = 1;
    services = {
      loungepi = {
        protected = true;
        registrationConfigFile = pkgs.writeText "gitlab-registration" ''
          CI_SERVER_URL=https://gitlab.muc.ccc.de
          REGISTRATION_TOKEN=8aR7kTXfbyKXcaAWk5Pa
        '';
        executor = "shell";
        tagList = [ "colmena-apply-local" "loungepi" ];
      };
    };
  };

  services.resolved.llmnr = "false";
  services.avahi = {
    enable = true;
    publish = {
      enable = true;
      addresses = true;
      domain = true;
      hinfo = true;
      userServices = true;
    };
  };

  systemd.user.services.librespot = {
    wantedBy = [ "default.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.librespot}/bin/librespot -n loungepi -v --backend pulseaudio";
      Restart = "always";
      RestartSec = "1s";
    };
    unitConfig = {
      StartLimitIntervalSec = "0";
    };
  };

  systemd.user.services.shairport-sync = {
    wantedBy = [ "default.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.shairport-sync}/bin/shairport-sync -v -o pa";
      Restart = "always";
    };
  };

  systemd.services.pr0get = {
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [
      bash curl jq 
    ];
    serviceConfig = {
      User = "lounge";
      ExecStart = ''/home/lounge/pr0get/pr0get.sh -d /home/lounge/pr0get/video -l 84 -c "pkill mpv" video'';
    };
  };
}
