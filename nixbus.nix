{ pkgs, ... }:

{
  imports = [ ./nixbus/user.nix ];

  boot = {
    loader.grub = {
      device = "/dev/vda";
      splashImage = null;
    };
    supportedFilesystems = [
      "xfs"
    ];
    kernelParams = [ "console=ttyS0" "console=tty0" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/vda2";
      fsType = "xfs";
      options = [
        "noatime"
        "nodiratime"
        "logbufs=8"
      ];
    };
  };

  swapDevices = [
    { device = "/dev/vda3"; }
  ];

  nix = {
    buildCores = 2;
    maxJobs = 5;
  };

  networking = {
    hostName = "nixbus";
    domain = "club.muc.ccc.de";
    hostId = "14e4f63f";
    usePredictableInterfaceNames = false;
    extraHosts = ''
      # host
      2001:7f0:3003:beef::67 nixbus.club.muc.ccc.de nixbus
      83.133.178.67 nixbus.club.muc.ccc.de nixbus
    '';
    firewall = {
      enable = true;
      allowPing = true;
      checkReversePath = true;
      allowedTCPPorts = [
        22
        80
        443
      ];
    };
    useDHCP = false;
    interfaces = {
      "eth0" = {
        ipv6.addresses = [
          { address = "2001:7f0:3003:beef::67"; prefixLength = 64; }
        ];
        ipv4.addresses = [
          { address = "83.133.178.67"; prefixLength = 26; }
        ];
      };
      #"eth0.2396" = {
      #  ipv6.addresses = [
      #    { address = "2001:7f0:3003:235e::98"; prefixLength = 64; }
      #  ];
      #  ip4 = [
      #    { address = "83.133.179.98"; prefixLength = 27; }
      #  ];
      #};
      #"eth0.2428" = {
      #  ipv6.addresses = [
      #    { address = "2001:7f0:3003:235f::130"; prefixLength = 64; }
      #  ];
      #  ip4 = [
      #    { address = "83.133.179.130"; prefixLength = 25; }
      #  ];
      #};
    };
    #vlans = {
    #  "eth0.2396" = {
    #    id = 2396;
    #    interface = "eth0";
    #  };
    #  "eth0.2428" = {
    #    id = 2428;
    #    interface = "eth0";
    #  };
    #};
    nameservers = [
      # XXX: currently not working
      #"2001:7f0:3003:beef::65"
      "83.133.178.65"
    ];
    defaultGateway6 = {
      address = "2001:7f0:3003:beef::65";
      metric = 42;
    };
    defaultGateway = {
      address = "83.133.178.65";
      metric = 42;
    };
    #localCommands = ''
    #  ${pkgs.iproute}/bin/ip -6 rule add from 2001:7f0:3003:235e::/64 table 2
    #  ${pkgs.iproute}/bin/ip -6 route add table 2 2001:7f0:3003:235e::/64 dev eth0.2396
    #  ${pkgs.iproute}/bin/ip -6 route add table 2 default via 2001:7f0:3003:235e::97 dev eth0.2396
    #  ${pkgs.iproute}/bin/ip -4 rule add from 83.133.179.96/27 table 2
    #  ${pkgs.iproute}/bin/ip -4 route add table 2 83.133.179.96/27 dev eth0.2396
    #  ${pkgs.iproute}/bin/ip -4 route add table 2 default via 83.133.179.97 dev eth0.2396
    #  ${pkgs.iproute}/bin/ip -6 rule add from 2001:7f0:3003:235f::/64 table 3
    #  ${pkgs.iproute}/bin/ip -6 route add table 3 2001:7f0:3003:235f::/64 dev eth0.2428
    #  ${pkgs.iproute}/bin/ip -6 route add table 3 default via 2001:7f0:3003:235f::129 dev eth0.2428
    #  ${pkgs.iproute}/bin/ip -4 rule add from 83.133.179.128/25 table 3
    #  ${pkgs.iproute}/bin/ip -4 route add table 3 83.133.179.128/25 dev eth0.2428
    #  ${pkgs.iproute}/bin/ip -4 route add table 3 default via 83.133.179.129 dev eth0.2428
    #'';
  };

  # MuCCC API
  services.nginx = {
    enable = true;
    virtualHosts."nixbus.club.muc.ccc.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = "return 404;";
      locations."/api/" = {
        proxyPass = "http://[::1]:8020/";
        #extraConfig = ''
        #  set_real_ip_from 83.133.178.64/26;
        #  set_real_ip_from 2001:7f0:3003:beef::/64;
        #  real_ip_header X-Forwarded-For;
        #'';
      };
    };
    virtualHosts."api.muc.ccc.de" = {
      locations."/".proxyPass = "http://[::1]:8020";
    };
  };

  systemd.services.muccc-api = {
    wantedBy = [ "multi-user.target" ];
    description = "MuCCC API";
    serviceConfig = {
      ExecStart = "${pkgs.muccc-api}/bin/muccc-api";
      DynamicUser = true;
      PrivateTmp = true;
    };
  };
}
