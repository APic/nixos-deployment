{
  description = "MuCCC Infrastructure Deployment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/master";
    flake-utils.url = "github:numtide/flake-utils";
    muccc-api = {
      url = "git+https://gitlab.muc.ccc.de/muCCC/api";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    luftschleuse2 = {
      url = "github:muccc/luftschleuse2/luftschleuse3";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, muccc-api, luftschleuse2, home-manager, nixos-generators, ... }: let
    supportedSystems = flake-utils.lib.defaultSystems;
  in flake-utils.lib.eachSystem supportedSystems (system: let
    pkgs = import nixpkgs { inherit system; };
  in rec {
    defaultPackage = pkgs.colmena;
    devShell = pkgs.mkShell {
      packages = with pkgs; [ colmena ];
    };
  }) // {
    sdimage.aarch64-linux = nixos-generators.nixosGenerate {
      pkgs = import nixpkgs { system = "aarch64-linux"; };
      format = "sd-aarch64";
    };
  } // {
    colmena = {
      meta = {
        nixpkgs = import nixpkgs { };
        specialArgs.flakes = {
          inherit self nixpkgs muccc-api luftschleuse2 home-manager;
        };
      };

      briafzentrum = { name, nodes, pkgs, ... }: {
        imports = [
          ./modules/default.nix
          "${nixpkgs}/nixos/modules/profiles/qemu-guest.nix"
          ./briafzentrum.nix
        ];
      };

      nixbus = { name, nodes, pkgs, ... }: {
        deployment.targetHost = "${name}.club.muc.ccc.de";
        imports = [
          ./modules/default.nix
          "${nixpkgs}/nixos/modules/profiles/qemu-guest.nix"
          ./nixbus.nix
        ];
      };

      loungepi = { name, nodes, pkgs, ... }: {
        deployment.allowLocalDeployment = true;
        nixpkgs.system = "aarch64-linux";
        imports = [
          ./modules/default.nix
          home-manager.nixosModules.home-manager
          ./loungepi.nix
        ];
      };

      luftschleuse = { name, nodes, pkgs, ... }: {
        deployment.allowLocalDeployment = true;
        nixpkgs.system = "aarch64-linux";
        imports = [
          ./modules/default.nix
          ./luftschleuse.nix
        ];
      };
    };
  };
}
