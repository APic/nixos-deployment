{ name, pkgs, lib, flakes, ... }:

{
  deployment.targetHost = lib.mkDefault "${name}.muc.ccc.de";
  networking.hostName = lib.mkDefault name;

  time.timeZone = "UTC";

  # between mkOptionDefault and mkDefault (on in rpi flake)
  boot.kernelPackages = lib.mkOverride 1001 pkgs.linuxPackages_latest;
  boot.tmpOnTmpfs = true;
  zramSwap.enable = true;

  environment.systemPackages = with pkgs; [
    wget curl htop iftop tmux tcpdump rsync git
    alacritty.terminfo
  ];
  documentation.nixos.enable = false;
  programs.bash.enableCompletion = true;
  programs.vim.defaultEditor = true;
  programs.zsh.enable = true;
  programs.mtr.enable = true;

  services.journald.extraConfig = ''
    SystemMaxUse=200M
    MaxRetentionSec=3d
  '';

  services.openssh = {
    enable = true;
    passwordAuthentication = lib.mkDefault false;
  };
  services.fail2ban.enable = lib.mkDefault true;

  services.nginx = {
    package = pkgs.nginxMainline;
    recommendedOptimisation = true;
    recommendedTlsSettings = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
    appendHttpConfig = ''
    access_log syslog:server=unix:/dev/log;
    '';
    appendConfig = ''
    error_log stderr info;
    '';
  };

  # little more prio then mkDefault because this is set by eval-config for currentSystem
  nixpkgs.system = lib.mkOverride 999 "x86_64-linux";
  nixpkgs.overlays = [
    flakes.muccc-api.overlay
    flakes.luftschleuse2.overlay
  ];

  # include git rev of this repo/flake into the nixos-version
  system.configurationRevision = flakes.nixpkgs.lib.mkIf (flakes.self ? rev) flakes.self.rev;
  system.nixos.revision = flakes.nixpkgs.rev;
  system.nixos.versionSuffix = "-${lib.substring 0 8 flakes.nixpkgs.rev}";
  # set nixpkgs on the target to the nixpkgs version of the deployment
  nix.registry.nixpkgs.flake = flakes.nixpkgs;
  nix.nixPath = lib.mkForce [ "nixpkgs=${flakes.nixpkgs}" "nixos-config=/dontuse" ];

  security.acme.email = "fpletz@muc.ccc.de";
  security.acme.acceptTerms = true;

  nix.gc = {
    automatic = true;
    dates = "daily";
    randomizedDelaySec = "1h";
    options = "--delete-older-than 7d";
  };
}
