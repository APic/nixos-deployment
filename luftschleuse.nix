{ config, pkgs, lib, ... }:

let
  sshCommandUsers = [
    { user = "open";
      msg = "Unlocking back door";
      cmd = "unlock";
    }
    { user = "openfront";
      msg = "Unlocking front door";
      cmd = "unlockfront";
    }
    { user = "close";
      msg = "Unlocking back door";
      cmd = "lock";
    }
  ];

in
{
  boot = {
    kernelPackages = pkgs.linuxPackages;
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
    };
    initrd.availableKernelModules = [ "vc4" ];
    kernelParams = [ "console=tty0" ];
  };

  hardware.enableRedistributableFirmware = false;
  hardware.firmware = [ pkgs.firmwareLinuxNonfree ];

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = [ "noatime" ];
    };
    "/boot/firmware = {
      device = "/dev/disk/by-label/FIRMWARE";
      fsType = "vfat";
    };
  };

  networking = {
    hostName = "luftschleuse";
    useNetworkd = true;
    useDHCP = false;
    interfaces.eth0.useDHCP = true;
    interfaces.wlan0 = {
      ipv4.addresses = [ { address = "192.168.2.2"; prefixLength = 24; } ];
    };
    firewall.trustedInterfaces = [ "wlan0" "eth0" ];
  };

  systemd.network.networks."40-wlan0" = {
    linkConfig.RequiredForOnline = false;
  };
  systemd.network.networks."40-eth0" = {
    linkConfig.RequiredForOnline = true;
  };

  environment.systemPackages = with pkgs; [ colmena lm_sensors ];

  services.fail2ban.enable = false;

  services.openssh.extraConfig = lib.concatMapStrings (t: ''
    Match User ${t.user}
      ForceCommand ${pkgs.writeScript "${t.user}.sh" ''
        #!${pkgs.stdenv.shell}
        set -o errexit
        echo '${t.msg}...'
        echo '${t.cmd}' | ${pkgs.netcat}/bin/nc -w 0 -u 127.0.0.1 2323
        echo 'Command sent.'
      ''}
  '') sshCommandUsers;

  users = {
    mutableUsers = false;
    users.root.openssh.authorizedKeys.keys = [
      # FIXME
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFJY+/tAXZFm9U+nJt0kKo6e/TrYiH7E49n0ktbuF5I6 fpletz@fpine"
    ];
    users.open = {
      isNormalUser = true;
      group = "users";
      openssh.authorizedKeys.keys = config.users.users.root.openssh.authorizedKeys.keys;
    };
    users.openfront = {
      isNormalUser = true;
      group = "users";
      openssh.authorizedKeys.keys = config.users.users.root.openssh.authorizedKeys.keys;
    };
    users.close = {
      isNormalUser = true;
      group = "users";
      openssh.authorizedKeys.keys = config.users.users.root.openssh.authorizedKeys.keys;
    };
  };

  systemd.services.dnsmasq = {
    after = [ "sys-subsystem-net-devices-wlan0.device" ];
    bindsTo = [ "sys-subsystem-net-devices-wlan0.device" ];
  };
  services.dnsmasq = {
    enable = true;
    resolveLocalQueries = false;
    extraConfig = ''
      bind-interfaces
      interface=wlan0
      bogus-priv
      no-resolv
      dhcp-range=192.168.2.10,192.168.2.200,30m
      no-ping
      dhcp-option=option:router
      dhcp-option=option:dns-server
    '';
  };

  services.hostapd = {
    enable = true;
    interface = "wlan0";
    countryCode = "DE";
    ssid = "luftschleuse3";
    channel = 7;
    wpa = false;
  };

  systemd.services.lockd = let lockdCfg = pkgs.writeText "lockd.cfg" ''
    [Front Door]
    type = door
    address = A
    key = 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    inital_unlock = False
    sequence_number_container_file = /tmp/front_door_rx_seq.log
    rx_sequence_leap = 32768
    timeout = 2

    [Back Door]
    type = door
    address = B
    key = 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    inital_unlock = True
    sequence_number_container_file = /tmp/back_door_rx_seq.log
    rx_sequence_leap = 32768
    timeout = 2

    [Master Controller]
    serialdevice = /dev/null
    baudrate = 115200

    [Master Controller Buttons]
    down = 2
    closed = 1
    member = 4
    public = 8

    [Master Controller LEDs]
    down = 0
    closed = 1
    member = 2

    [Logging]
    host = 127.0.0.1
    port = 23514
    level = debug

    [Display]
    display_type = Nokia_1600
    max_update_rate = .5

    [Status Receiver]
    host = 127.0.0.1
    port = 2080
  ''; in {
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      Restart = "always";
      ExecStart = "${pkgs.luftschleuse2-lockd}/bin/lockd ${lockdCfg}";
    };
  };
}
