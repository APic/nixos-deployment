FROM nixos/nix:latest

RUN echo 'experimental-features = nix-command flakes' >> /etc/nix/nix.conf
RUN nix-env -iA nixpkgs.git

ADD . /src
WORKDIR /src

RUN nix run . -- build -v
